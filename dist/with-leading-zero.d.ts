/**
 * Добавляет ноль в начале числа от 0 до 9
 *
 * @param  {string | number} num
 * @returns string
 */
export declare function withLeadingZero(num: string | number): string;
