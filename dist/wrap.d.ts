interface WrapOptions {
    class?: string;
}
/**
 * Оборачивает элемент в другой элемент.
 * Аналог метода "wrap" в jQuery.
 *
 * @param  {Element} element - DOM-элемент
 * @param  {string} wrapNodeType - например, 'div'
 * @param  {WrapOptions} options
 * @returns Element | void
 */
export declare function wrap(element: Element, wrapNodeType?: string, options?: WrapOptions): Element | void;
export {};
