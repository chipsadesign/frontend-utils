/**
 * Генерирует событие на указанном DOM-элементе.
 *
 * @param  {Element|Document|Window} element - DOM-элемент, на котором генерируется событие.
 * @param  {string} eventName - название события
 */
export declare function triggerEvent(element: Element | Document | Window, eventName: string): void;
/**
 * Генерирует пользовательское событие на указанном DOM-элементе.
 *
 * @param  {Element|Document|Window} element - DOM-элемент, на котором генерируется событие.
 * @param  {string} eventName - название события
 * @param  {any} data - передаваемые данные.
 */
export declare function triggerCustomEvent(element: Element | Document | Window, eventName: string, data?: any): void;
