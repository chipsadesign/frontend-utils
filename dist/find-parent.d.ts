/**
 * @param  {string} selector - селектор, по которому производится поиск элемента
 * @param  {Element} startingElement - элемент, с которого начинается поиск
 * @returns {Element | false}
 */
export declare function findParent(selector: string, startingElement: Element): false | Element;
