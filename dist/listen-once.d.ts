/**
 * Слушает событие только до первого триггера.
 * Аналог метода "one" в jQuery.
 *
 * @param  {Element | Document | Window} element - DOM-элемент, который слушаем
 * @param  {string} eventName - название события
 * @param  {EventListener} callback
 */
export declare function listenOnce(element: Element | Document | Window, eventName: string, callback: (event?: Event) => void): void;
