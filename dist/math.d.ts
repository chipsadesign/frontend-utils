/**
 * Возвращает случайное целое число от {min} до {max}
 *
 * @param  {number} min
 * @param  {number} max
 * @returns number
 */
export declare function randomInt(min: number, max: number): number;
/**
 * Возвращает случайное дробное число от {min} до {max}
 *
 * @param  {number} min
 * @param  {number} max
 * @returns number
 */
export declare function randomFloat(min: number, max: number): number;
