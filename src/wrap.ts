interface WrapOptions {
    class?: string;
}

/**
 * Оборачивает элемент в другой элемент.
 * Аналог метода "wrap" в jQuery.
 *
 * @param  {Element} element - DOM-элемент
 * @param  {string} wrapNodeType - например, 'div'
 * @param  {WrapOptions} options
 * @returns Element | void
 */
export function wrap(element: Element, wrapNodeType = 'div', options: WrapOptions = {}): Element | void {
    const parent = element.parentNode;
    const wrapper = document.createElement(wrapNodeType);

    if (options.class) {
        wrapper.classList.add(options.class);
    }

    if (parent) {
        parent.insertBefore(wrapper, element);
        wrapper.appendChild(element);
        return wrapper;
    }
}
