/**
 * Добавляет ноль в начале числа от 0 до 9
 *
 * @param  {string | number} num
 * @returns string
 */
export function withLeadingZero(num: string | number): string {
    const number = typeof num === 'string' ? parseFloat(num) : num;
    return number < 10 ? `0${number}` : `${number}`;
}
