/**
 * @param  {string} selector - селектор, по которому производится поиск элемента
 * @param  {Element} startingElement - элемент, с которого начинается поиск
 * @returns {Element | false}
 */
export function findParent(selector: string, startingElement: Element) {
    let parent = startingElement;

    while (!parent.matches(selector)) {
        if (!parent.parentElement) return false;
        parent = parent.parentElement;
    }

    return parent;
}
