/**
 * Возвращает случайное целое число от {min} до {max}
 *
 * @param  {number} min
 * @param  {number} max
 * @returns number
 */
export function randomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Возвращает случайное дробное число от {min} до {max}
 *
 * @param  {number} min
 * @param  {number} max
 * @returns number
 */
export function randomFloat(min: number, max: number): number {
    return min + Math.random() * (max - min);
}
