/**
 * Генерирует событие на указанном DOM-элементе.
 *
 * @param  {Element|Document|Window} element - DOM-элемент, на котором генерируется событие.
 * @param  {string} eventName - название события
 */
export function triggerEvent(element: Element | Document | Window, eventName: string) {
    let event;

    if (window.event) {
        try {
            event = new Event(eventName, { bubbles: true, cancelable: false });
        } catch (err) {
            event = document.createEvent('HTMLEvents');
            event.initEvent(eventName, true, false);
        }
    } else {
        event = document.createEvent('HTMLEvents');
        event.initEvent(eventName, true, false);
    }

    element.dispatchEvent(event);
}

/**
 * Генерирует пользовательское событие на указанном DOM-элементе.
 *
 * @param  {Element|Document|Window} element - DOM-элемент, на котором генерируется событие.
 * @param  {string} eventName - название события
 * @param  {any} data - передаваемые данные.
 */
export function triggerCustomEvent(element: Element | Document | Window, eventName: string, data?: any) {
    let event;

    if (window.CustomEvent) {
        try {
            event = new CustomEvent(eventName, { detail: data });
        } catch (err) {
            event = document.createEvent('CustomEvent');
            event.initCustomEvent(eventName, true, true, data);
        }
    } else {
        event = document.createEvent('CustomEvent');
        event.initCustomEvent(eventName, true, true, data);
    }

    element.dispatchEvent(event);
}
