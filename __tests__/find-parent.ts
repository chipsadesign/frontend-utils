import { findParent } from '../src/find-parent';

describe('findParent', () => {
    it('successfully finds a parent element', () => {
        document.body.innerHTML = `
            <div data-testid="parent-element">
                <div data-testid="starting-element"></div>
            <div>
        `;

        const startingElement = document.querySelector('[data-testid="starting-element"]') as Element;
        const parentElement = document.querySelector('[data-testid="parent-element"]') as Element;

        expect(findParent('[data-testid="parent-element"]', startingElement)).toEqual(parentElement);
    });

    it('starting element can be treated as target element', () => {
        document.body.innerHTML = `
            <div data-testid="starting-element"></div>
        `;

        const startingElement = document.querySelector('[data-testid="starting-element"]') as Element;

        expect(findParent('[data-testid="starting-element"]', startingElement)).toEqual(startingElement);
    });

    it('returns falsey value if parent element not found', () => {
        document.body.innerHTML = '';

        expect(findParent('[data-testid="non-existing-element"]', document.body)).toBeFalsy();
    });
});
