import { withLeadingZero } from '../src/with-leading-zero';

describe('withLeadingZero', () => {
    it('correctly works with one-digit number', () => {
        const num = 1;
        expect(withLeadingZero(num)).toEqual('01');
    });

    it('gcorrectly works with two-digit number', () => {
        const num = 12;
        expect(withLeadingZero(num)).toEqual('12');
    });
});
