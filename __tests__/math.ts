import { randomFloat, randomInt } from '../src/math';

describe('Random number generator', () => {
    it('generates random float within specified boundaries', () => {
        const num = randomFloat(1, 3);
        expect(num).toBeGreaterThanOrEqual(1);
        expect(num).toBeLessThanOrEqual(3);
    });

    it('generates random integer within specified boundaries', () => {
        const num = randomInt(1, 10);
        expect(Number.isInteger(num)).toEqual(true);
        expect(num).toBeGreaterThanOrEqual(1);
        expect(num).toBeLessThanOrEqual(10);
    });
});
