import { triggerEvent, triggerCustomEvent } from '../src/trigger-event';

describe('Triggerring events', () => {
    it('triggers usual event', () => {
        const fn = jest.fn();
        document.addEventListener('click', fn);
        triggerEvent(document, 'click');
        expect(fn).toHaveBeenCalledTimes(1);
    });

    it('triggers custom event', () => {
        const fn = jest.fn();
        document.addEventListener('custom-event', fn);
        triggerCustomEvent(document, 'custom-event');
        expect(fn).toHaveBeenCalledTimes(1);
    });
});
