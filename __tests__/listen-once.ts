import { listenOnce } from '../src/listen-once';
import { triggerEvent } from '../src/trigger-event';

it('Triggers callback just once', () => {
    const fn = jest.fn();
    listenOnce(document, 'click', fn);
    triggerEvent(document, 'click');
    expect(fn).toHaveBeenCalledTimes(1);
});
