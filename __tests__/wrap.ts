import { wrap } from '../src/wrap';

it('Wraps an element with an another element', () => {
    const element = document.createElement('span');
    document.body.appendChild(element);
    const wrapper = wrap(element, 'span', { class: 'test' });
    expect(element.parentElement).toEqual(wrapper);
    expect(element.parentElement!.classList.contains('test')).toEqual(true);
});
